//type1
function type1(){
    console.log('type 1 functions');
}
type1();
//type2
function type2(n='hiii'){
    console.log(n);
}
type2('hello');
//type3 anonymous functions
let type3 = function(n){
    console.log(n);
}
type3('hii');
//type4 ILFE -> immedietly invocation function expression
(function(){
    console.log('hiii');
}());